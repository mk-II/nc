#!/usr/bin/python3
#-*- coding: utf-8 -*-

import numpy as np

m = 1.0
k = 2.0
dt = 0.1

def F(x, t):
    ans = -k*x;
    return ans

def E(x, v):
    K = 0.5*m*v**2
    U = 0.5*k*x**2
    return K + U

def main():
    x = 2.0
    v = 0.0
    x_new = 0.0
    v_new = 0.0
    f = open('euler_1D.csv', 'w')

    for t in np.arange(0, 50, dt):
        f.write(f'{t},{v},{x},{E(x, v)}\r\n')

        x_new = x + dt*v
        v_new = v + dt*F(x, t)/m

        x = x_new
        v = v_new

    f.close()

if __name__=='__main__':
    main()
