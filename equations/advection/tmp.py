#!/usr/bin/python3
#-*- coding: utf-8 -*-

from numpy.fft import fft, rfft, irfft, fftfreq
import numpy as np
import pickle


# Constant
L = 20.0 # x軸長さ
T = 10
N = 256 # 格子数(FFTでは2**nとなる値である必要がある)
#m = N//2 + 1 # 波数の大きさ
m = N # 波数の大きさ
dt = 1.0e-3
dx = L/N
c = 10.0 # speed

x = np.linspace(0, L, N)
t = np.arange(0, T, dt)
k = fftfreq(n=m, d=0.2)*m*2*np.pi/L # 波数

# Initial conds.
#u0 = np.cos(np.pi*x)
u0 = np.zeros_like(x)
u0[5:50] = 2.0



# RHS of equations(波数空間)
def conv_k(t, u):
    #v = fft(u) # フーリエ変換
    #return -1.0j*k * u
    v = fft(u) # フーリエ変換
    return -1.0j*k * v

# RHS of equations(実空間)
def conv(t, u):
    du = np.convolve(u, [3.0, -4.0, 1.0], 'valid')/(2.0*dx)
    return np.hstack([0.0, 0.0, -du])

def euler(func, T, y0):
    dt = (T[-1] - T[0])/len(T)
    y = np.zeros([len(T), len(y0)])
    y[0] = y0

    for t in range(len(T[:-1])):
        y[t+1] = y[t] + dt*func(t,y[t])

    return y

def RK4(func, T, y0):
    dt = (T[-1] - T[0])/len(T)
    y = np.zeros([len(T), len(y0)])
    y[0] = y0

    #for t in range(len(T[:-1])):
    for i,t in enumerate(T[:-1]):
        k1 = func(t, y[i])
        k2 = func(t+dt/2, y[i]+k1*dt/2)
        k3 = func(t+dt/2, y[i]+k2*dt/2)
        k4 = func(t+dt, y[i]+k3*dt)
        y[i+1] = y[i] + (k1 + 2*k2 + 2*k3 + k4)*dt/6.0

    return y


#u = euler(func=conv, T=t, y0=u0)
u = RK4(func=conv, T=t, y0=u0)
with open('tmp_u.pkl', 'wb') as f:
    pickle.dump(u, f)
