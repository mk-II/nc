#!/usr/bin/python3
#-*- coding: utf-8 -*-
#
# 1次元移流方程式
# 空間 : 1次精度風上差分
# 時間 : 5次Adams-Bashforth法

import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np


def f(x):
    '''矩形波'''
    if 5.0 < x < 10.0:
        return 2.0
    else:
        return 0.0

def main():
    a = 20; b = 5
    dx = 0.1
    dt = 0.001
    c = 10.0 # speed
    x = np.arange(0, a, dx)
    t = np.arange(0, b, dt)
    u = np.zeros([len(t), len(x)])
    u2 = np.zeros([len(t), len(x)])
    exact = np.zeros([len(t), len(x)])

    # 初期条件
    for i in range(len(x)):
        u[0][i] = f(x[i])
        u2[0][i] = f(x[i])
        exact[0][i] = f(x[i])
    # 境界条件(Dirichlet)
    u[0][0] = 0; u[0][-1] = 0
    u2[0][0] = 0; u2[0][-1] = 0

    def rhs(i, j):
        '''移流方程式の右辺(1次精度風上差分)'''
        return -c/dx*(u[i][j] - u[i][j-1])

    print('CFL (c*dt/dx)= {}'.format(c*dt/dx))
    for i in range(len(t)-1): # t
        u[i][0] = 0; u[i][-1] = 0 # Dirichlet BC
        u2[i][0] = 0; u2[i][-1] = 0 # Dirichlet BC
        for j in range(1, len(x)-1): # x
            k1 = 1901.0*rhs(i, j)
            k2 = -2774.0*rhs(i-1, j)
            k3 = 2616.0*rhs(i-2, j)
            k4 = -1274.0*rhs(i-3, j)
            k5 = 251.0*rhs(i-4, j)
            u[i+1][j] = u[i][j] + dt/720.0*(k1 + k2 + k3 + k4 + k5)

            u2[i+1][j] = u2[i][j] - c*dt/dx*(u2[i][j]-u2[i][j-1])
            exact[i][j] = f(x[j] - c*t[i])

    fig = plt.figure()
    frame = len(t)
    def update(i):
        if i != 0:
            fig.clear()
        plt.ylim([-2, 4])
        plt.plot(x, u[i], color='blue', ls='-', label='AB2')
        plt.plot(x, u2[i], color='red', ls=':', label='Euler')
        plt.plot(x, exact[i], color='black', ls='--', label='exact')
        plt.title('time step = {}'.format(i))
        plt.legend()

    ani = animation.FuncAnimation(fig, update, interval=1, frames=frame)
    fig.show()
    ani.save('output.gif', writer='imagemagick')

if __name__=='__main__':
    main()

