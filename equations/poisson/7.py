#!/usr/bin/python3
#-*- coding: utf-8 -*-

from scipy.sparse import diags, lil_matrix, linalg
import matplotlib.pyplot as plt
import numpy as np
import sys


# 2次元ポアソン方程式(行列化SOR, scipyによる高速化)
def main():
    # Constant
    e0 = 8.85e-12 # 真空中の誘電率
    EPS = 1.0e-05
    eps = 1.0
    a = 1
    dx = 0.01
    dy = 0.01
    M = int(a/dx)-2
    N = int(a/dy)-2
    omega = 1.8

    phi_new = np.zeros([M, N]).flatten()
    phi = np.zeros_like(phi_new)
    rho = np.zeros([M, N])

    print('dx**2 = %lf' % (dx**2))
    # 電荷を配置
    for j in range(M): # x
        for k in range(N): # y
            if ((M/2-j)**2 + (N/2-k)**2)*dx**2 < 0.05**2:
                rho[j][k] = 1.0e-08

    Im = np.eye(M)
    In = np.eye(N)
    Jm = diags([1, 1], [-1, 1], shape=(M, N)).toarray()
    Jn = diags([1, 1], [-1, 1], shape=(M, N)).toarray()

    A = np.kron(In, (2*Im - Jm)/dx**2) + np.kron((2*In - Jn)/dy**2, Im)
    D = np.diagflat(np.diag(A))
    #D_inv = np.reciprocal(D) # D_inv = np.linalg.inv(D)と等価(Dは対角行列のため)
    D_inv = np.reciprocal(D, where=D!=0)
    D_inv[D_inv == np.inf] = 0 # 上のreciprocalで0除算が発生するため
    L = np.tril(A) - D
    U = np.triu(A) - D
    b = -rho.flatten()/e0 # b = -rho.flatten()/e0

    A.astype(np.float32)
    D.astype(np.float32)
    L.astype(np.float32)
    U.astype(np.float32)
    b.astype(np.float32)

    A = lil_matrix(A).tocsc()
    D = lil_matrix(D).tocsc()
    L = lil_matrix(L).tocsc()
    U = lil_matrix(U).tocsc()
    b = lil_matrix(b).T.tocsc()

    tmp = linalg.inv(D + omega*L) # tmp = spsolve(D + omega*L, np.eye(N**2))
    Q = np.dot(tmp, (1-omega)*D - omega*U)
    P = omega*tmp

    Q.astype(np.float32)
    P.astype(np.float32)

    tmp = lil_matrix(tmp).tocsc()
    Q = lil_matrix(Q).tocsc()
    P = lil_matrix(P).tocsc()
    phi_new = lil_matrix(phi_new).T.tocsc()
    phi = lil_matrix(phi).T.tocsc()

    cnt = 0
    while EPS < eps:
        if cnt % 100 == 0:
            print(f'cnt = {cnt}, eps = {eps:e}')

        phi_new = Q.dot(phi) + P.dot(b)

        eps = np.linalg.norm(phi_new.toarray() - phi.toarray(), np.inf)
        phi = phi_new
        cnt += 1

    print('cnt = {}, eps = {}'.format(cnt, eps))
    print(f'cnt = {cnt}, eps = {eps:e}')

    phi_new = phi_new.toarray()

    plt.imshow(phi_new.reshape(M, N), cmap='coolwarm')
    pp = plt.colorbar(orientation='vertical')
    pp.set_label('phi [V]')
    plt.show()

if __name__=='__main__':
    main()

