#!/usr/bin/python3
#-*- coding: utf-8 -*-

from scipy import sparse
from scipy.sparse import linalg
import numpy as np
import sys


# RHS of Poisson eq.
def f():
    pass


# 2次元Poisson方程式(scipy.sparse.linalg.spsolveで解く)
class PoissonSolver2D(object):
    def __init__(self, b0, bc):
        self.W = b0.shape[0]
        self.H = b0.shape[1]
        dx = 1.0/self.W
        dy = 1.0/self.H

        Im = sparse.eye(self.W)
        In = sparse.eye(self.H)
        Jm = sparse.diags([1, 1], [-1, 1], shape=(self.W, self.H))
        Jn = sparse.diags([1, 1], [-1, 1], shape=(self.W, self.H))

        self.A = -sparse.kron(In, (2*Im - Jm)/dx**2) - sparse.kron((2*In - Jn)/dy**2, Im)
        self.b = -b0.flatten().T

        # Boundary conditions
        idx = np.where(bc == 0)
        for x,y in zip(idx[1], idx[0]):
            n = x + y*self.W
            self.A[n,n] = 1.0
            #self.b[n] = 0.0

        #print(self.W, self.H, dx, dy)
        #print(self.A.shape, self.b.shape)

    def solve(self):
        phi = sparse.linalg.spsolve(self.A, self.b)
        return phi.reshape(self.W, self.H) 


if __name__=='__main__':
    b0 = np.loadtxt('input_IC.txt')
    bc = np.loadtxt('input_BC.txt')

    solver = PoissonSolver2D(b0, bc)
    result = solver.solve()

    np.savetxt('result.txt', result)
