#!/usr/bin/python3
#-*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
import pickle
import sys


def f(x, y, a=1.0, mu=0.0, sigma=1.0): # 2次元ガウス分布
    return a*np.exp(-((x-mu)**2 + (y-mu)**2)/(2.0/sigma**2))

# Burgers equation(2次元)
# 時間項 : オイラー陽解法
# 移流項 : 風上差分
# 拡散項 : 2次精度中心差分
def main():
    a = 30; b = 30; c = 15
    dx = 0.5
    dy = 0.5
    dt = 0.05
    nu = 0.5
    x = np.arange(0, a, dx)
    y = np.arange(0, b, dy)
    t = np.arange(0, c, dt)
    u = np.zeros([len(t), len(x), len(y)])
    v = np.zeros([len(t), len(x), len(y)])

    r1 = dt/dx; r2 = dt/dy; r3 = nu*dt/dx**2; r4 = nu*dt/dx**2
    # 安定性解析
    print('r1 = %lf' % (r1))
    print('r2 = %lf' % (r2))
    print('r3 = %lf' % (r3))
    print('r4 = %lf' % (r4))

    # 境界条件
    u[:][0][:], u[:][-1][:], u[:][:][0], u[:][:][-1] = 0, 0, 0, 0
    v[:][0][:], v[:][-1][:], v[:][:][0], v[:][:][-1] = 0, 0, 0, 0

    # 初期条件
    X, Y = np.meshgrid(x, y)
    u[0] = f(X, Y, a = 1.0, mu=15, sigma=0.5) # t = 0
    v[0] = f(X, Y, a = 1.0, mu=15, sigma=0.5) # t = 0
    for j in range(1, len(x)-1): # t = 1
        for k in range(1, len(y)-1):
            C1 = 1 + r1*u[0][j][k] + r2*v[0][j][k] - 2*(r3+r4)
            C2 = r3 - r1*u[0][j][k]
            C3 = r4 - r2*v[0][j][k]

            u[1][j][k] = C1*u[0][j][k] + C2*u[0][j+1][k] + C3*u[0][j][k+1] + r3*u[0][j-1][k] + r4*u[0][j][k-1]
            v[1][j][k] = C1*v[0][j][k] + C2*v[0][j+1][k] + C3*v[0][j][k+1] + r3*v[0][j-1][k] + r4*v[0][j][k-1]

    progress = (len(t)-1)*(len(x)-1)*(len(y)-1)
    cnt = 0
    for i in range(1, len(t)-1): # t
        for j in range(1, len(x)-1): # x
            for k in range(1, len(y)-1): # y
                C1 = 1 + r1*u[i][j][k] + r2*v[i][j][k] - 2*(r3+r4)
                C2 = r3 - r1*u[i][j][k]
                C3 = r4 - r2*v[i][j][k]

                u[i+1][j][k] = C1*u[i][j][k] + C2*u[i][j+1][k] + C3*u[i][j][k+1] + r3*u[i][j-1][k] + r4*u[i][j][k-1]

                v[i+1][j][k] = C1*v[i][j][k] + C2*v[i][j+1][k] + C3*v[i][j][k+1] + r3*v[i][j-1][k] + r4*v[i][j][k-1]

                cnt += 1
                sys.stdout.write('\r{:.3f} %'.format(cnt/progress*100))
                sys.stdout.flush()

    print('')

    with open('2d_1.pkl', 'wb') as result:
        pickle.dump(u, result)

if __name__=='__main__':
    main()

