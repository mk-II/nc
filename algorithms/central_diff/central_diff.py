#!/usr/bin/python3
#-*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np


def f(x):
    return np.cos(x)

def main():
    y = np.array([])
    h = 0.1
    y0 = 0

    t = np.arange(0, 50, 2*h)
    for i in t:
        y0 += 2*h*f(i-h)
        y = np.append(y, y0)

    plt.plot(t, y)
    plt.plot(t, np.sin(t)) # 解析解
    plt.show()

if __name__=='__main__':
    main()

